<?php
namespace Models {
    class Client
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }
        public function finds($id)
        {
            $sql = "SELECT * FROM productos WHERE id = '$id';";
            return $this->connection->runQuery($sql)[0];
        }

        public function stadistics($id_client)
        {
            $sql = "SELECT count(id), coalesce(sum(precio),0) FROM compras WHERE id_cliente = '$id_client';";
            return $this->connection->runQuery($sql)[0];
        }

        public function addToCart($id)
        {
            $id_usuario = $_SESSION["usuario_id"];
            $sql = "INSERT INTO carrito(id_usuario,id_producto) VALUES ('$id_usuario', '$id');";
            $this->connection->runStatement($sql);
        }
        
        public function childs($id)
        {
            $sql = "SELECT * FROM categorias WHERE id_categoria_padre = '$id';";
            return $this->connection->runQuery($sql);
        }
        
        public function showProductsAll()
        {
            $sql = "SELECT * FROM productos ORDER BY id;";
            return $this->connection->runQuery($sql);
        }
        
        public function showParentCategories()
        {
            $sql = "SELECT * FROM categorias WHERE id_categoria_padre = 0;";
            return $this->connection->runQuery($sql);
        }
        
        public function find($id)
        {
            $sql = "SELECT * FROM categorias WHERE id = '$id';";
            return $this->connection->runQuery($sql)[0];
        }

        public function showProducts($id)
        {
            $sql = "SELECT * FROM productos WHERE id_categoria = '$id';";
            return $this->connection->runQuery($sql);
        }

        public function showCart($id)
        {
            $sql = "SELECT p.nombre, p.precio, c.id FROM carrito c, productos p WHERE c.id_producto = p.id and c.id_usuario = '$id';";
            return $this->connection->runQuery($sql);
        }

        public function buy($id)
        {
            $sql = "SELECT * FROM productos WHERE id = '$id' and stock > 0;";
            $result = $this->connection->runQuery($sql);

            if ($result) {
                $stock = intval($result[0]["stock"]) - 1;
                $sql = "UPDATE productos SET stock = '$stock' WHERE id = '$id';";
                $this->connection->runStatement($sql);

                $id_usuario = $_SESSION["usuario_id"];
                $precio = $result[0]["precio"];
                $nombre = $result[0]["nombre"];
                $descripcion = $result[0]["descripcion"];
                $imagen = $result[0]["imagen"];
                $sql = "INSERT INTO compras(id_cliente, precio, nombre, descripcion, imagen) VALUES ('$id_usuario', '$precio', '$nombre', '$descripcion', '$imagen');";
                $this->connection->runStatement($sql);

                $sql = "DELETE FROM carrito WHERE id_producto = '$id' and id_usuario = '$id_usuario';";
                $this->connection->runStatement($sql);
                return true;
            }
            else {
                return false;
            }
        }

        public function cartUsers($id)
        {
            $sql = "SELECT c.*, p.nombre FROM carrito c LEFT JOIN productos p ON (c.id_producto = p.id) WHERE c.id_usuario = '$id';";
            return $this->connection->runQuery($sql);
        }

        public function findProduct($id)
        {
            $sql = "SELECT * FROM carrito WHERE id = '$id';";
            return $this->connection->runQuery($sql)[0];
        }

        public function deleteWish($id)
        {
            $sql = "DELETE FROM carrito WHERE id = '$id';";
            return $this->connection->runStatement($sql);
        }

        public function showHistorical($id)
        {
            $sql = "SELECT * FROM compras WHERE id_cliente = '$id';";
            return $this->connection->runQuery($sql);
        }
    }
}