<?php

namespace Models {
    class Admin
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function stadistics()
        {
            $sql = "SELECT (SELECT count(*) FROM usuarios) AS clientes, (SELECT count(*) FROM compras) AS productos, (SELECT coalesce(sum(precio), 0) FROM compras) AS total;";
            return $this->connection->runQuery($sql)[0];
        }

        public function showCategories()
        {
            $sql = "SELECT * FROM categorias ORDER BY id;";
            return $this->connection->runQuery($sql);
        }

        public function showProducts()
        {
            $sql = "SELECT * FROM productos ORDER BY id;";
            return $this->connection->runQuery($sql);
        }

        public function showParentCategories()
        {
            $sql = "SELECT * FROM categorias WHERE id_categoria_padre = 0;";
            return $this->connection->runQuery($sql);
        }

        public function id($id)
        {
            $sql = "SELECT usuario FROM usuarios WHERE id = '$id';" ;
            return $this->connection->runQuery($sql)[0];
        }

        public function insertCategory($nombre, $padre)
        {
            $sql = "INSERT INTO categorias (id_categoria_padre, nombre) 
                                    VALUES ('$padre', '$nombre');";
            $this->connection->runStatement($sql);
        }

        public function insertProduct($sku, $nombre, $descripcion, $idcategoria, $stock, $precio, $imagen )
        {
            $sql = "INSERT INTO productos (sku, nombre, descripcion, id_categoria, stock, precio, imagen)
                                VALUES ('$sku', '$nombre', '$descripcion', '$idcategoria', '$stock', '$precio', '$imagen');";
            $this->connection->runStatement($sql);
        }

        public function find($id)
        {
            $sql = "SELECT * FROM categorias WHERE id = '$id';";
            return $this->connection->runQuery($sql)[0];
        }

        public function finds($id)
        {
            $sql = "SELECT * FROM productos WHERE id = '$id';";
            return $this->connection->runQuery($sql)[0];
        }

        public function stock($id)
        {
            $sql = "SELECT * FROM productos WHERE stock < '$id';";
            return $this->connection->runQuery($sql);
        }

        public function deleteCategory($id)
        {
            $sql = "DELETE FROM categorias WHERE id = '$id'";
            $this->connection->runStatement($sql);
        }

        public function deleteProduct($id)
        {
            $sql = "DELETE FROM productos WHERE id = '$id'";
            $this->connection->runStatement($sql);
        }

        public function updateCategory($id, $nombre , $padre)
        {
            $sql = "UPDATE categorias SET nombre = '$nombre', categoria_padre = '$padre' WHERE id = '$id';";
            $this->connection->runStatement($sql);
        }

        public function updateProduct($id, $sku, $nombre, $descripcion, $idcategoria, $stock, $precio, $imagen)
        {
            if ($imagen) {
                $sql = "UPDATE productos SET sku = '$sku', nombre = '$nombre', descripcion = '$descripcion', id_categoria = '$idcategoria', stock = '$stock', precio = '$precio', imagen = '$imagen' WHERE id = '$id';";
                $this->connection->runStatement($sql);
            }
            else {
                $sql = "UPDATE productos SET sku = '$sku', nombre = '$nombre', descripcion = '$descripcion', id_categoria = '$idcategoria', stock = '$stock', precio = '$precio' WHERE id = '$id';";
                $this->connection->runStatement($sql);   
            }
        }
    }
}