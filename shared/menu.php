
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand text-light" href="#">E-Shop</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php
        require_once __DIR__ . '/sessions.php';
        $menu = [];
        if (isset($_SESSION['usuario_id']) || !empty($_SESSION['usuario_id'])) {
          if ($_SESSION['usuario_admin'] == '1') {
            $menu = [
            'Home' => '/Admin',
            'Products' => '../Admin/products.php',
            'Categories' => '../Admin/categories.php',
            ];
          }
          else  {
            $menu = [
            'Home' => '/Client',
            'Products' => '../Client/products_client.php',
            'Historical' => '../Client/historical.php',
            'Wishes' => '../Client/wishes.php'
            ];
          }
          foreach ($menu as $key => $value) {
            echo "<li class='nav-item'>
                    <a class='nav-link text-light' href='$value'>$key</a>
                  </li>";
          }
        }
        ?>
    </ul>
    <?php
        if (isset($_SESSION['usuario_id']) || !empty($_SESSION['usuario_id'])) {
        ?>
    <ul class="nav navbar-nav navbar-right">
      <li><a href='/seguridad/logout.php'class="btn btn-danger">Cerrar </a></li>
    </ul>
        <?php
        }
        ?>
  </div>
</nav>


