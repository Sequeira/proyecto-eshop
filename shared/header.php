<?php
    require_once __DIR__ . '/menu.php';
    ?>
<!DOCTYPE html>
  <html>
  <head>
    <title class="font-weight-light"><?=$title?></title><br>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/doc.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/login.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/principal.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/nav.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/registro.css">
  </head>
  <body>
    
