<?php

require_once __DIR__ . '/../Db/MySqlConnection.php';
require_once __DIR__ . '/../Models/Usuario.php';
require_once __DIR__ . '/../Models/Admin.php';
require_once __DIR__ . '/../Models/Client.php';

use Db\MySqlConnection;
$con = new MySqlConnection('root', 'root', 'e_shop', 3306, 'localhost');
$con->connect();

$usuario_model = new Models\Usuario($con);
$admin_model = new Models\Admin($con);
$client_model = new Models\Client($con);