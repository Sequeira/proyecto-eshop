DROP TABLE IF EXISTS compras;
DROP TABLE IF EXISTS usuarios;
DROP TABLE IF EXISTS productos;
DROP TABLE IF EXISTS categorias;

CREATE TABLE usuarios(
	id INT NOT NULL AUTO_INCREMENT,
	nombre TEXT NOT NULL,
	primerApellido TEXT NOT NULL,
	segundoApellido TEXT NOT NULL,
	telefono INT NOT NULL,
	direccion TEXT NOT NULL,
	usuario TEXT NOT NULL,
	pass TEXT NOT NULL,
	admin BOOLEAN DEFAULT FALSE NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE categorias(
	id INT NOT NULL AUTO_INCREMENT,
	id_categoria_padre INT,
	nombre TEXT,
	PRIMARY KEY (id)
);

CREATE TABLE productos(
	id INT NOT NULL AUTO_INCREMENT,
	sku INT NOT NULL,
	nombre TEXT,
	descripcion TEXT,
	imagen TEXT,
	id_categoria INT,
	stock INT,
	precio DOUBLE,
	PRIMARY KEY (id),
	FOREIGN KEY (id_categoria) REFERENCES categorias(id)
);

CREATE TABLE carrito(
	id INT NOT NULL AUTO_INCREMENT,
	id_usuario INT,
	id_producto INT,
	cantidad TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (id_usuario) REFERENCES usuarios(id),
	FOREIGN KEY (id_producto) REFERENCES productos(id)
);

CREATE TABLE compras(
	id INT NOT NULL AUTO_INCREMENT,
	id_cliente INT,
	precio DOUBLE,
	nombre TEXT,
	descripcion TEXT,
	imagen TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (id_cliente) REFERENCES usuarios(id)
);

INSERT INTO usuarios (nombre, primerApellido, segundoApellido, direccion, telefono, usuario, pass, admin) VALUES
					('Sandra', 'Sequeira', 'Reyes', 'Los Chiles', 22222222, 'Admin', 'Admin123', TRUE);