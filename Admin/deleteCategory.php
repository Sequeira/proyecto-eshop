<?php
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
$title = 'Eliminar Categoría';
require_once '../shared/header.php';
require_once '../shared/db.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$category = $admin_model->find($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $admin_model->deleteCategory($id);
    return header('Location: /Admin/categories.php');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <p>¿Seguro que quiere eliminar la categoría= <?=$id?></p>
  <form method="POST">
    <input class="btn btn-success" type="submit" value="Aceptar">
    <a class="btn btn-default btn-danger" href="/Admin/categories.php">Cancelar</a>
  </form>
</div>