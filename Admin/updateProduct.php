<?php 
$title = 'Actualizar Producto';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/header.php';
require_once '../shared/db.php';
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$category = $admin_model->finds($id);
$categories = $admin_model->showParentCategories();
$sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING);
$nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
$descripcion = filter_input(INPUT_POST, 'descripcion', FILTER_SANITIZE_STRING);
$idcategoria = filter_input(INPUT_POST, 'idcategoria', FILTER_SANITIZE_STRING);
$stock = filter_input(INPUT_POST, 'stock', FILTER_SANITIZE_STRING);
$precio = filter_input(INPUT_POST, 'precio', FILTER_SANITIZE_STRING);
$imagen = filter_input(INPUT_POST, 'imagen', FILTER_SANITIZE_STRING);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if ($imagen) {
		$formatos   = array('.jpg', '.png', '.gif');
		$directorio = '../archivos'; 
		$nombreArchivo    = $_FILES['imagen']['name'];
		$nombreTmpArchivo = $_FILES['imagen']['tmp_name'];
		$ext              = substr($nombreArchivo, strrpos($nombreArchivo, '.'));
		if (in_array($ext, $formatos)){
			if (move_uploaded_file($nombreTmpArchivo, "$directorio/$nombreArchivo")){
				echo "Felicitaciones, archivo $nombreArchivo subido exitosamente";
				echo "<h3>$nombreArchivo</h3>"; 
				echo "Imagen"; //imprime: ¡Saludos! 
				echo "<input type='text' name='imagen' value='$nombreArchivo' > "; 

			}else{
				echo 'Ocurrió un error subiendo el archivo, valida los permisos de la carpeta "archivos"';
			}
		}else{
			echo 'Esta extension no es permitida para una foto';
		}	
	}
	$admin_model->updateProduct($id,$sku,$nombre,$descripcion,$idcategoria,$stock,$precio,$imagen);
	return header('Location: /Admin/products.php');

}	
?>
<form class="container" method="POST">
  <div class="form-group">
    <label>SKU</label>
    <input type="text" class="form-control" name="sku" value="<?=$category['sku']?>" placeholder="Category Names">
    <label>Name</label>
    <input type="text" class="form-control" name="nombre" value="<?=$category['nombre']?>" placeholder="Category Names">
    <label>Descripcion</label>
    <input type="text" class="form-control" name="descripcion" value="<?=$category['descripcion']?>" placeholder="Category Names">
    <label>Category</label>
    <select class="form-control" name="idcategoria"><br>
    	<?php 
    		if ($categories) {
    			foreach ($categories as $item) {
    				echo "<option value=" . $item['id'] . ">"  . $item['nombre'] . "</option>";
    			}
    		}
    	 ?>
    </select>
    <label>Stock</label>
    <input type="text" class="form-control" name="stock" value="<?=$category['stock']?>" placeholder="Category Names">
    <label>Descripcion</label>
    <input type="text" class="form-control" name="precio" value="<?=$category['precio']?>" placeholder="Category Names">
	<label> imagen</label>
	
    <label for="archvio">Archivo</label>
	<input type="file" class="form-control-file" id="archvio" aria-describedby="fileHelp" name="imagen" value="<?=$category['imagen']?>">
	
  </div>
  <div class="form-group">
  
  </div>
  <button type="submit" class="btn btn-primary" name="botonx">Actualizar producto</button>
</form>
