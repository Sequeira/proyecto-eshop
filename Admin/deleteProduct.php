<?php
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
$title = 'Eliminar Producto';
require_once '../shared/header.php';
require_once '../shared/db.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$category = $admin_model->finds($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $admin_model->deleteProduct($id);
    return header('Location: /contact_us.php');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <p>¿Está seguro de querer eleminar este producto?= <?=$id?></p>
  <form method="POST">
    <input class="btn btn-success" type="submit" value="Aceptar">
    <a class="btn btn-default btn-danger" href="../contact_us.php">Cancelar</a>
  </form>
</div>