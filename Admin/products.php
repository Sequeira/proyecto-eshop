<?php
	$title = 'Producto';
	require_once '../shared/header.php';
	require_once '../shared/db.php';
	require_once '../shared/sessions.php';
	require_once '../shared/guard.php';
	$products = $admin_model->showProducts();
	$categories = $admin_model->showCategories();
 
	$sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING);
	$nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
	$descripcion = filter_input(INPUT_POST, 'descripcion', FILTER_SANITIZE_STRING);
	$idcategoria = filter_input(INPUT_POST, 'idcategoria', FILTER_SANITIZE_STRING);
	$stock = filter_input(INPUT_POST, 'stock', FILTER_SANITIZE_STRING);
	$precio = filter_input(INPUT_POST, 'precio', FILTER_SANITIZE_STRING);
	//$imagen 
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$formatos   = array('.jpg', '.png', '.gif');
		$directorio = '../archivos'; 
		$nombreArchivo    = $_FILES['archivo']['name'];
		$nombreTmpArchivo = $_FILES['archivo']['tmp_name'];
		$ext              = substr($nombreArchivo, strrpos($nombreArchivo, '.'));
		if (in_array($ext, $formatos)){
			if (move_uploaded_file($nombreTmpArchivo, "$directorio/$nombreArchivo")){
				echo "Felicitaciones, archivo $nombreArchivo subido exitosamente";
				echo "<h3>$nombreArchivo</h3>"; 
				echo "Imagen"; //imprime: ¡Saludos! 
				echo "<input type='text' name='imagen' value='$nombreArchivo' > "; 

			}else{
				echo 'Ocurrió un error subiendo el archivo, valida los permisos de la carpeta "archivos"';
			}
		}else{
			echo 'Esta extension no es permitida para una foto';
		}
		$admin_model->insertProduct($sku,$nombre,$descripcion,$idcategoria,$stock,$precio,$nombreArchivo);
		return header('Location: /Admin/products.php');

	}	
?>

<form class="container" method="POST" enctype="multipart/form-data">
	<label></label>
	<label>SKU</label><br>
	<input class="form-control" type="text" name="sku"><br>
	<label>Name</label><br>
	<input class="form-control" type="text" name="nombre"><br>
	<label>Descripcion</label><br>
	<input class="form-control" type="text" name="descripcion"><br>
	<label>Category</label><br>
	<select class="form-control" name="idcategoria"><br>
    	<?php 
    		if ($categories) {
    			foreach ($categories as $category) {
    				echo "<option value=" . $category['id'] . ">"  . $category['nombre'] . "</option>";
    			}
    		}
    	 ?>
    </select>
	<label>Stock</label><br>
	<input class="form-control" type="text" name="stock"><br>
	<label>Price</label><br>
	<input class="form-control" type="text" name="precio"><br>
	<label for="archvio">Picture</label>
		<input type="file" class="form-control-file" id="archvio" aria-describedby="fileHelp" name="archivo"><br>
	<button type="submit" class="btn btn-primary" name="botonx">Subir producto</button><br><br><br><br><br>
</form>

<div class="container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Name of product</th>
          <th>Sku</th>
		  <th>Foto</th>
		  <th>idCategoria</th>
		  <th>Stock</th>
		  <th>Precio</th>
		  <th>Descripcion</th>
		  <th>Options</th>
        </tr>
        <?php
        if ($products) {
            foreach ($products as $fila) {
                require './product_row.php';
            }
        } else {
            echo '<tr><td class="text-center" colspan=7>No hay datos</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>  
</div>