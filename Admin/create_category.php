<?php 
$title = 'Create category';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/header.php';
require_once '../shared/db.php';
$categories = $admin_model->showParentCategories();
$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$parent = filter_input(INPUT_POST, 'parent', FILTER_SANITIZE_STRING);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $admin_model->insertCategory($name,$parent);
    return header('Location: ./categories.php');
}
 ?>
<form class="container" method="POST">
  <div class="form-group">
    <label>Nombre  de la categoría</label>
    <input type="text" class="form-control" name="name" value="<?=$name?>" placeholder="Nombre">
  </div>
  <div class="form-group">
    <label>Descripción</label>
    <select class="form-control" name="parent">
    	<?php 
    		echo "<option value='0'>Parent</option>";
    		if ($categories) {
    			foreach ($categories as $category) {
    				echo "<option value=" . $category['id'] . ">"  . $category['nombre'] . "</option>";
    			}
    		}
    	 ?>
    </select>
  </div>
  <button type="submit" class="btn btn-dark">Crear</button>
</form>

 