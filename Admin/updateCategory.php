<?php 
$title = 'Actualizar Categoría';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/header.php';
require_once '../shared/db.php';
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$category = $admin_model->find($id);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $parent = filter_input(INPUT_POST, 'parent', FILTER_SANITIZE_STRING);
    $admin_model->updateCategory($id, $name, $parent);
    return header('Location: /Admin/categories.php');
}
?>
<form class="container" method="POST">
  <div class="form-group">
    <label>Category Name</label>
    <input type="text" class="form-control" name="name" value="<?=$category['nombre']?>" placeholder="Category Names">
  </div>
  <div class="form-group">
    <label>Parent Category</label>
    <select class="form-control" name="parent">
    	<?php 
    		echo "<option value='parent'>Parent</option>";
    		if ($categories) {
    			foreach ($categories as $category) {
    				echo "<option value=" . $category['nombre'] . ">"  . $category['nombre'] . "</option>";
    			}
    		}
    	 ?>
    </select>
  </div>
  <button type="submit" class="btn btn-dark">Actualizar</button>
</form>
