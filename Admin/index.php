<?php
$title = 'Administrator ';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$stadistics = $admin_model->stadistics();
?>
<div class="container">
  <h1><?=$title?></h1>
  <form action="/Admin/send.php">
    <!--<input type="number" required name="number">
    <button type="submit" class="btn btn-dark">Low stock</button>-->
  </form>
  

  <div class="row">
    <div class="col-md-12">
    
      <table class="table  table-striped table-bordered  table-hover ">
        <tr>
          <th>Estadistica</th>
          <th>Cantidad</th>
        </tr>
        <?php
        if ($stadistics) {
            echo "<tr>";
            echo "<td>Cantidad de usuarios</td>";
            echo "<td>" . $stadistics['clientes'] . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Total de productos</td>";
            echo "<td>" . $stadistics['productos'] . "</td>";
            echo "</tr>";
             echo "<tr>";
            echo "<td>Cantidad de ventas</td>";
            echo "<td>" . $stadistics['total'] . "</td>";
            echo "</tr>";
        } else {
            echo '<tr><td class="text-center" colspan=2>No hay datos</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>
</div>