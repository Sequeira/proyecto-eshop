  <?php 
$title = 'Categorias Administrator';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/db.php';

$categories = $admin_model->showCategories();
 ?>
<div class="container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Categoría</th>
          <th>Descripción</th>
          <th class="text-right"><a class="btn bg-dark text-light" href="./create_category.php">Crear</a></th>
        </tr>
        <?php
        if ($categories) {
            foreach ($categories as $fila) {
                require './category_row.php';
            }
        } else {
            echo '<tr><td class="text-center" colspan=2>No hay datos</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>  
</div>