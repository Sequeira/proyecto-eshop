<?php

namespace Db {
    use Mysqli;

    require_once __DIR__ . '/DbConnection.php';
    class MySqlConnection extends DbConnection
    {
        private $connection;
        public function __construct($user, $password, $database, $port, $server)
        {
            parent::__construct($user, $password, $database, $port, $server);
        }

        public function connect()
        {
            $this->connection = new mysqli($this->server, $this->user, $this->password, $this->database);
        }

        public function disconnect()
        {
            $this->connection->close();
        }

        public function runQuery($sql)
        {
            $stmt = $this->runStatement($sql);
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }

        public function runStatement($sql)
        {
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            return $stmt;
        }
    }
}
