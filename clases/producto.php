<?php
require "conexion.php";

class Producto{
    public $nombre;
    public $descripcion;
    public $imagen;
    public $categoria;
    public $stock;
    public $precio;

    function __construct($nombre,$descripcion,$imagen,$categoria,$stock,$precio){
        $this->nombre=$nombre;
        $this->descripcion=$descripcion;
        $this->imagen=$imagen;
        $this->categoria=$categoria;
        $this->stock=$stock;
        $this->precio=$precio;
    }
    
}


?>