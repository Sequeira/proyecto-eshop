 <?

require "conexion.php";

class Usuario{
    public $nombre;
    public $primerApellido;
    public $segundoApellido;
    public $direccion;
    public $telefono;
    public $usuario;
    public $pass;
    public $rol;

   function  __construct($nombre,$primerApellido,$segundoApellido,$direccion,$telefono,$usuario,$pass,$rol){
       $this->nombre=$nombre;
       $this->primerApellido=$primerApellido;
       $this->segundoApellido=$segundoApellido;
       $this->direccion=$direccion;
       $this->telefono=$telefono;
       $this->usuario=$usuario;
       $this->pass=$pass;
       $this->rol=$rol;
   }
}
 ?>
