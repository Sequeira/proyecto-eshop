<?php

  $title = 'Contact Us';
  require_once '../shared/header.php';
  require_once '../shared/db.php';
  require_once '../shared/sessions.php';
  require_once '../shared/guard.php';
  $products = $client_model->showHistorical($_SESSION['usuario_id']);
?>
<div class="container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Nombre</th>
          <th>Descripción</th>
          <th>Precio</th>
        </tr>
        <?php
        if ($products) {
            foreach ($products as $fila) {
                require './historical_row.php';
            }
        } else {
            echo '<tr><td class="text-center" colspan=7>No hay datos</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>  
</div>