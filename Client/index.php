<?php
$title = 'Clientes ';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$stadistics = $client_model->stadistics($_SESSION['usuario_id']);
?>
  <link rel="stylesheet" type="text/css" href="/assets/css/cliente.css">
<div class="container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Estadística</th>
          <th>Cantidad</th>
        </tr>
        <?php
        if ($stadistics) {
            echo "<tr>";
            echo "<td>Productos comprados</td>";
            echo "<td>" . $stadistics['count'] . "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Total de compras</td>";
            echo "<td>" . $stadistics['coalesce'] . "</td>";
            echo "</tr>";
        } else {
            echo '<tr><td class="text-center" colspan=2>No hay datos</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>
</div>