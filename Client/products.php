<?php

  $title = 'Contact Us';
  require_once '../shared/header.php';
  require_once '../shared/header.php';
	require_once '../shared/db.php';
	require_once '../shared/sessions.php';
  require_once '../shared/guard.php';
  $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
  $products = $client_model->showProducts($id);
?>
<div class="container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Nombre del producto</th>
          <th>Sku</th>
		  <th>Foto</th>
		  <th>idCategoria</th>
		  <th>Stock</th>
		  <th>Precio</th>
		  <th>Descripcion</th>
		  <th>Options</th>
        </tr>
        <?php
        if ($products) {
            foreach ($products as $fila) {
                require './product_row.php';
            }
        } else {
            echo '<tr><td class="text-center" colspan=7>No hay datos</td></tr>';
        }
        ?>
      </table>
    </div>
  </div>  
</div>