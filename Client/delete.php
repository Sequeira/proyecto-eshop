<?php
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
$title = 'Delete Category';
require_once '../shared/header.php';
require_once '../shared/db.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$category = $client_model->findProduct($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $client_model->deleteWish($id);
    return header('Location: /Client');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <p>¿Are you shure about delete the wish with the id= <?=$id?></p>
  <form method="POST">
    <input class="btn btn-success" type="submit" value="Aceptar">
    <a class="btn btn-default btn-danger" href="/Client/wishes.php">Cancelar</a>
  </form>
</div>