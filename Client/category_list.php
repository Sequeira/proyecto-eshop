0<?php 	
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';
 ?>
<ul class="list-group">
  <li class="list-group-item"><a href="./products.php?id=<?=$category['id']?>"><?=$category['nombre']?></a>
  	<?php
  		$childs = $client_model->childs($category['id']);
  		if ($childs) {
  			echo "<ul>";
  			foreach ($childs as $child) {
  				echo "<li><a href='./products.php?id=" . $child['id'] . "'>" . $child['nombre'] .  "</a></li>";
  			}
  			echo "</ul>";
  		}
  	 ?>
  </li>
</ul>