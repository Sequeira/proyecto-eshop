<?php 
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/db.php';
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$category = $client_model->finds($id);
$title = $category['nombre'];
require_once '../shared/header.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$client_model->addToCart($id);
	return header('Location: /contact_us.php');
}


?>
<div class="container d-flex">
	<img src="/archivos/<?=$category['imagen']?>" height="400" width="400">
	<form class="container" method="POST">
	  <div class="form-group">
	    <label>SKU</label>
	    <input type="text" readonly class="form-control-plaintext" name="sku" value="<?=$category['sku']?>" placeholder="Category Names">
	    <label>Name</label>
	    <input type="text" readonly class="form-control-plaintext" name="nombre" value="<?=$category['nombre']?>" placeholder="Category Names">
	    <label>Descripcion</label>
	    <input type="text" readonly class="form-control-plaintext" name="descripcion" value="<?=$category['descripcion']?>" placeholder="Category Names">
	    <label>ID</label>
	    <input type="text" readonly class="form-control-plaintext" name="idcategoria" value="<?=$category['id_categoria']?>" placeholder="Category Names">
	    <label>Stock</label>
	    <input type="text" readonly class="form-control-plaintext" name="stock" value="<?=$category['stock']?>" placeholder="Category Names">
	    <label>Descripcion</label>
	    <input type="text" readonly class="form-control-plaintext" name="precio" value="<?=$category['precio']?>" placeholder="Category Names">
	  </div>
	  <button type="submit" class="btn btn-primary" name="botonx">Añadir a lista de deseos</button>  
	</form>
</div>





