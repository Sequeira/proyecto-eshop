<?php
require_once '../shared/sessions.php';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';
$cartItems = $client_model->cartUsers($_SESSION['usuario_id']);
$failed = [];
if ($cartItems) {
	foreach ($cartItems as $items) {
		if ($client_model->buy($items['id_producto'])) {
			
		}
		else {
			array_push($failed, $items['nombre']);
		}
		
	}
}
if ($failed) {
	echo "<div class='container'>";
	echo "<p>Los productos ";
	foreach ($failed as $fail) {
		echo $fail;
	}
	echo " no pudieron ser comprados por carencia de stock y deben de ser eliminados del carrito manualmente</p>";
	echo "</div>";
	echo "<a class='btn btn-dark' href='./wishes.php'>Back to cart</a>";
}
else{
	return header('Location: /Client');
}
?>
