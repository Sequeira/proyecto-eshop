<?php
$title = 'Clients View';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$categories = $client_model->showParentCategories();
foreach ($categories as $category) {
	require './category_list.php';
}
?>