<?php
$title = 'Home';
require_once './shared/header.php';
require_once './shared/sessions.php';

if (!isset($_SESSION['usuario_id']) || empty($_SESSION['usuario_id'])) {
    return header('Location: /seguridad/login.php');
}
if ($_SESSION['usuario_admin'] == 't') {
	return header('Location: /Admin');
}
else {
	return header('Location: /Client');
}

require_once './shared/footer.php';