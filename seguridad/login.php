<?php
$title = 'Login';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/sessions.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = $_POST['usuario'];
    $pass = $_POST['password'];
    $user = $usuario_model->login($usuario, $pass);
    if ($user != null) {
          $_SESSION['usuario_id'] = $user['id'];
          $_SESSION['usuario_admin'] = $user['admin'];
          if ($user['admin'] == '1') {
            return header('Location: /Admin');
          }
          else {
            return header('Location: /Client');
          }
    } else { 
          echo "<h3>Usuario o contraseña inválido!</h3>";
    }
}
?>

<div class="container">
    <div class ="login">
        <div class="row d-flex justify-content-center mx-auto">
            <div class="col-md-6 col-xs-12 div-style">
            <form method="POST">
                <div class="d-flex justify-content-center mx-auto main-label" >
                    <h1>Login</h1>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control text-box" name="usuario" aria-describedby="" placeholder="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control text-box" name="password" placeholder="">
                </div>
                <div class="form-group justify-content-center d-flex">
                    <button type="submit"   Value="Login" class="btn btn-primary button-submit">Login</button>
                </div>
                <a class="btn btn-default" href="/seguridad/signup.php">Registrar cuenta</a>
            </form>
           </div>
        </div>
    </div>
</div>
</form>