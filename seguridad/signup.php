<?php
$title = 'Registro';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/sessions.php';



if($_SERVER['REQUEST_METHOD']=='POST'){
$nombre = $_POST['nombre'];
$primerApellido = $_POST['primerApellido'];
$segundoApellido = $_POST['segundoApellido'];
$telefono = $_POST['telefono'];
$direccion = $_POST['direccion'];
$usuario = $_POST['usuario'];
$pass = $_POST['pass'];
$pass_verificacion = $_POST['pass_verificacion'];
$pass_verificacion = $_POST['pass_verificacion'];
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($password == $password_verification) {
        $usuario_model->insert($nombre, $primerApellido, $segundoApellido, $telefono, $direccion, $usuario, $pass);
        return header('Location: /seguridad/login.php');
    }
    echo "<h3>Contraseñas no coinciden</h3>";
}
?>

<form method="POST">
    <div class="well">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                <input type="text" name="nombre"  class="form-control" placeholder="Nombre">
                </div>
            </div>
             <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="direccion">Dirección:</label>
                    <input type="text" name="direccion" class="form-control" placeholder="Dirección">
                </div>
            </div>
             <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="apellido1">Primer apellido:</label>
                    <input type="text" name="primerApellido" class="form-control" placeholder="Primer apellido">
                </div>
            </div>
             <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="telefono">Teléfono:</label>
                    <input type="text" name="telefono"  class="form-control" placeholder="Teléfono">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="apellido2">Segundo apellido:</label>
                    <input type="text" name="segundoApellido"  class="form-control" placeholder="Segundo apellido">
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="" id="" class="form-control" placeholder="Datos de la cuenta" tabindex="5" disabled>
        </div>
        <div class="form-group">
            <label for="">Usuario:</label>
            <input type="text" name="usuario" class="form-control" placeholder="usuario">
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" name="pass"  class="form-control" placeholder="Contraseña">
                </div>
                 <div class="form-group">
                    <label for="password2">Confrimar contraseña:</label>
                    <input type="password" name="pass_verificacion"class="form-control" placeholder="Contraseña">
                </div>

            </div>
        </div>
        <div class="row">
            <button class="btn btn-primary col-xs-6 col-md-6 nav-link btn btn-success" value="Login!" FONT SIZE=30 type="submit">Registrar</button>
            <a  class="nav-link btn btn-info col-xs-6 col-md-6 " href="/seguridad/logout.php" >Cerrar sesion</a>
        </div>
</form><br>