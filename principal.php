

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Pantalla principal</title>
    <link rel="stylesheet" href="css/principal.css"> 
<body>
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand font-weight-light .text-center " href="#">
   <h1><img src="images/usuario.png" width="60" height="60" class="d-inline-block align-center" alt="" loading="lazy">Administrador  <?php echo $_SESSION['nombre'] ?></h1> 
</nav>
<div class="menu">
    <ul class="nav nav-tabs ">
  <li class="nav-item navbar btn-toolbar ">
    <a class="nav-link  btn btn-secondary"id=pro href="#">Productos</a>
  </li>
  <li class="nav-item navbar btn-toolbar">
    <a class="nav-link btn btn-light" href="views/categoria.view.php">Categorias</a>
  </li>
  <li class="nav-item navbar btn-toolbar">
    <a class="nav-link btn btn-info " href="cerrar.php">Cerrar Sesion</a>
  </li>
</ul>	
</div>
</body>
</html>